##########################################################################
# XML Interface Project                                                  #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
# Project: xml_examples
# Filename: 01_create_simple_xml.4gl
# Created By: egza
# Creation Date: Nov 30, 2016

MAIN
  DEFINE document XMLDOCUMENT
  DEFINE root XMLNODE
  DEFINE comment_node XMLNODE
  DEFINE child_node1, child_node2 XMLNODE
  DEFINE p1, p2 XMLNODE
  DEFINE str STRING
  
  CALL document.create("root")
  
  LET root = document.getFirstDocumentNode()
  LET comment_node = document.createComment("Sample comment")
  
  LET p1 = document.createElement("burglar1")
  
  LET child_node1 = document.CreateElement("burglar")
  CALL child_node1.setAttribute("name", "Clyde")
  CALL child_node1.setAttribute("gender", "male")
  CALL child_node1.setNodeValue("19th April, 1932. Grocery store robbery")
  CALL p1.appendChild(child_node1)
  
  LET p2 = document.createElement("burglar2")
  
  LET child_node2 = document.createElement("burglar")
  CALL child_node2.setAttribute("name", "Bonnie")
  CALL child_node2.setAttribute("gender", "female")
  CALL child_node2.setNodeValue("19th April, 1932. Grocery store robbery")
  CALL p2.appendChild(child_node2)
  
  CALL root.appendChild(comment_node)
  CALL root.appendChild(p1)
  CALL root.appendChild(p2)

  CALL document.save("C:\\temp\\out.xml")
  DISPLAY "DONE"
END MAIN
##########################################################################
# XML Interface Project                                                  #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
# Project: xml_examples
# Filename: 02_editing_of_existing_xml.4gl
# Created By: egza
# Creation Date: Nov 30, 2016

MAIN
	DEFINE document XMLDOCUMENT
	DEFINE list, list1 XMLNODELIST
	DEFINE p1, root, node1, node2 XMLNODE
	DEFINE node_attribute XMLATTRIBUTE
	DEFINE str STRING
	
	CALL document.Create("")
	CALL document.Load("C:\\temp\\out.xml", FALSE)

	LET root = document.getFirstDocumentNode()
	LET node1 = root.getChildNodeItem(4)
	DISPLAY node1.getNodeName()
	LET node2 = node1.getChildNodeItem(2)
	CALL node2.setAttribute("name", "Mikkey")
	CALL node2.setNodeValue("1933. Acting in 'The Mad Doctor' movie")
	
	LET node1 = document.createElement("burglar3")
    LET node2 = document.createElement("burglar")
    CALL node2.setAttribute("name", "Mini")
    CALL node2.setAttribute("gender", "female")
    CALL node2.setNodeValue("1933. Acting in 'The Mad Doctor' movie")
    CALL node1.appendChild(node2)
	CALL root.appendChild(node1)
	
	CALL document.Save("C:\\temp\\out.xml")
END MAIN
##########################################################################
# XML Interface Project                                                  #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
# Project: xml_examples
# Filename: 03_filling_xml_from_record.4gl
# Created By: egza
# Creation Date: Nov 30, 2016

DEFINE arr DYNAMIC ARRAY OF RECORD
								cont_id INT,
  								cont_name STRING,
  								cont_address STRING,
  								cont_tel INT
  							END RECORD 

MAIN
  DEFINE document XMLDOCUMENT
  DEFINE root XMLNODE
  DEFINE child_node XMLNODE
  DEFINE parent_node XMLNODE
  DEFINE i INT
	
  
  CALL document.Create("Contacts")
  
  LET root = document.GetFirstDocumentNode()
  CALL root.setAttribute("year", 2016)
  CALL init_data()
  
  FOR i = 1 TO arr.getLength() 
	  LET parent_node = document.createElement("Contact")
	  CALL parent_node.setAttribute("id", arr[i].cont_id)
	  LET child_node = document.CreateElement("Name")
	  CALL child_node.SetNodeValue(arr[i].cont_name)
	  CALL parent_node.appendChild(child_node)
	  LET child_node = document.CreateElement("Address")
	  CALL child_node.SetNodeValue(arr[i].cont_address)
	  CALL parent_node.appendChild(child_node)
	  LET child_node = document.CreateElement("Phone")
	  CALL child_node.SetNodeValue(arr[i].cont_tel)
	  CALL parent_node.appendChild(child_node)
	  CALL root.AppendChild(parent_node)  
  END FOR

  CALL document.Save("C:\\temp\\out3.xml")
  DISPLAY "DONE"
END MAIN


FUNCTION init_data()
	DEFINE i INT

	FOR i = 1 TO 9
		LET arr[i].cont_id = i+10
		LET arr[i].cont_name = "Name "||i
		LET arr[i].cont_address = "Address "||i
		LET arr[i].cont_tel = fgl_random(1000000, 9999999)
		
	END FOR
	
END FUNCTION	
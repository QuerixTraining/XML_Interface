##########################################################################
# XML Interface Project                                                  #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
# Project: xml_examples
# Filename: 04_reading_xml_to_record.4gl
# Created By: egza
# Creation Date: Nov 30, 2016

DEFINE arr DYNAMIC ARRAY OF RECORD
								cont_id INT,
  								cont_name STRING,
  								cont_address STRING,
  								cont_tel INT
  							END RECORD 

MAIN
  DEFINE document XMLDOCUMENT
  DEFINE node, child_node XMLNODE
  DEFINE nodelist XMLNODELIST
  DEFINE value STRING
  DEFINE i INT
  
  CALL document.Create()
  CALL document.Load("C:\\temp\\out3.xml", FALSE)

  LET nodelist = document.getElementsByTagName('Contact')
  
  FOR i = 1 TO nodelist.getSize() 
	  LET node = nodelist.getItem(i)
	  LET arr[i].cont_id = i+10
	  LET child_node = node.getChildNodeItem(2)
	  LET arr[i].cont_name = child_node.getNodeValue()
	  LET child_node = node.getChildNodeItem(4)
	  LET arr[i].cont_address = child_node.getNodeValue()
	  LET child_node = node.getChildNodeItem(6)
	  LET arr[i].cont_tel = child_node.getNodeValue()
  END FOR
  
  FOR i = 1 TO arr.getSize()
  	  DISPLAY arr[i]
  END FOR	  
END MAIN

